﻿using System;
using System.Collections.Generic;
using System.Text;
using ZhCun.Framework.Common;

namespace AccountManager.Runtime
{
    public class AppConfig
    {
        public static int KeyMouseTimeOut
        {
            get
            { 
                string r = Configurations.GetAppSettings("KeyMouseTimeOut");
                if (string.IsNullOrEmpty(r))
                {
                    return 100;
                }
                return int.Parse(r);
            }
        }
    }
}
