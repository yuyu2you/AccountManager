﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccountManager.Runtime
{
    public class LoginInfo
    {
        static LoginInfoModel _loginInfo;

        public static void SetLoginInfo(LoginInfoModel loginInfo)
        {
            _loginInfo = loginInfo;
        }

        public static LoginInfoModel LoginState
        {
            get
            {
                return _loginInfo;
            }
        }

        public static string OperatorId
        {
            get
            {
                return _loginInfo.OperatorId;
            }
        }
        public static string UserName
        {
            get
            {
                return _loginInfo.Name;
            }
        }
        public static string LoginName
        {
            get
            {
                return _loginInfo.LoginName;
            }
        }
        public static DateTime LoginTime
        {
            get
            {
                return _loginInfo.LoginTime;
            }
        }
        public static bool IsManager
        {
            get
            {
                if (_loginInfo.OperatorId == "00")
                {
                    return true;
                }
                return false;
            }
        }

    }
}
