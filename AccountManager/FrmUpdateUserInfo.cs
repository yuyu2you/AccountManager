﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;
using AccountManager.Runtime;

namespace AccountManager
{
    public partial class FrmUpdateUserInfo : BaseForm
    {
        public FrmUpdateUserInfo()
        {
            InitializeComponent();
        }
        ALLogin _ALObj;

        private void FrmUpdateUserInfo_Load(object sender, EventArgs e)
        {
            _ALObj = new ALLogin();
            txtLoginName.Text = LoginInfo.LoginState.LoginName;
            txtUserName.Text = LoginInfo.UserName;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string newUserName = txtUserName.Text;
            string newLoginName = txtLoginName.Text;
            string errMsg;
            bool r = _ALObj.UpdateUserInfo(newUserName, newLoginName, out errMsg);
            if (r)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                ShowMessage(errMsg);
            }
        }

        private void txtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
