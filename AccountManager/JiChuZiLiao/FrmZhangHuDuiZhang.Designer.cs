﻿namespace AccountManager.JiChuZiLiao
{
    partial class FrmZhangHuDuiZhang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.txtActNamePY = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBalance = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNewBalance = new System.Windows.Forms.TextBox();
            this.dcm1 = new ZhCun.Framework.WinCommon.Components.DCM(this.components);
            this.txtConsumeType = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.txtAccount = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboxList1 = new ZhCun.Framework.WinCommon.Components.ComboxList(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(295, 242);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 26);
            this.btnClose.TabIndex = 21;
            this.btnClose.Text = "退出";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(55, 242);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 26);
            this.btnOK.TabIndex = 20;
            this.btnOK.Text = "确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // txtRemark
            // 
            this.comboxList1.SetColumns(this.txtRemark, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtRemark, 0);
            this.comboxList1.SetGridViewWidth(this.txtRemark, 0);
            this.dcm1.SetIsUse(this.txtRemark, true);
            this.comboxList1.SetIsUse(this.txtRemark, false);
            this.txtRemark.Location = new System.Drawing.Point(101, 172);
            this.txtRemark.Multiline = true;
            this.txtRemark.Name = "txtRemark";
            this.comboxList1.SetNextControl(this.txtRemark, this.txtRemark);
            this.txtRemark.ReadOnly = true;
            this.txtRemark.Size = new System.Drawing.Size(246, 58);
            this.txtRemark.TabIndex = 19;
            this.dcm1.SetTagColumnName(this.txtRemark, null);
            this.dcm1.SetTextColumnName(this.txtRemark, "Remark");
            // 
            // txtActNamePY
            // 
            this.comboxList1.SetColumns(this.txtActNamePY, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtActNamePY, 0);
            this.comboxList1.SetGridViewWidth(this.txtActNamePY, 0);
            this.dcm1.SetIsUse(this.txtActNamePY, true);
            this.comboxList1.SetIsUse(this.txtActNamePY, false);
            this.txtActNamePY.Location = new System.Drawing.Point(101, 142);
            this.txtActNamePY.Name = "txtActNamePY";
            this.comboxList1.SetNextControl(this.txtActNamePY, this.txtActNamePY);
            this.txtActNamePY.ReadOnly = true;
            this.txtActNamePY.Size = new System.Drawing.Size(246, 21);
            this.txtActNamePY.TabIndex = 18;
            this.dcm1.SetTagColumnName(this.txtActNamePY, null);
            this.dcm1.SetTextColumnName(this.txtActNamePY, "ActNamePY");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(60, 175);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 12);
            this.label3.TabIndex = 24;
            this.label3.Text = "备注:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 145);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 12);
            this.label1.TabIndex = 23;
            this.label1.Text = "助记码:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 12);
            this.label4.TabIndex = 25;
            this.label4.Text = "原余额:";
            // 
            // txtBalance
            // 
            this.comboxList1.SetColumns(this.txtBalance, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtBalance, 0);
            this.comboxList1.SetGridViewWidth(this.txtBalance, 0);
            this.dcm1.SetIsUse(this.txtBalance, true);
            this.comboxList1.SetIsUse(this.txtBalance, false);
            this.txtBalance.Location = new System.Drawing.Point(101, 51);
            this.txtBalance.Name = "txtBalance";
            this.comboxList1.SetNextControl(this.txtBalance, this.txtNewBalance);
            this.txtBalance.ReadOnly = true;
            this.txtBalance.Size = new System.Drawing.Size(247, 21);
            this.txtBalance.TabIndex = 26;
            this.dcm1.SetTagColumnName(this.txtBalance, null);
            this.dcm1.SetTextColumnName(this.txtBalance, "Balance");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(48, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 12);
            this.label5.TabIndex = 27;
            this.label5.Text = "新余额:";
            // 
            // txtNewBalance
            // 
            this.comboxList1.SetColumns(this.txtNewBalance, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtNewBalance, 0);
            this.comboxList1.SetGridViewWidth(this.txtNewBalance, 0);
            this.dcm1.SetIsUse(this.txtNewBalance, true);
            this.comboxList1.SetIsUse(this.txtNewBalance, false);
            this.txtNewBalance.Location = new System.Drawing.Point(101, 81);
            this.txtNewBalance.Name = "txtNewBalance";
            this.comboxList1.SetNextControl(this.txtNewBalance, this.txtNewBalance);
            this.txtNewBalance.Size = new System.Drawing.Size(246, 21);
            this.txtNewBalance.TabIndex = 2;
            this.dcm1.SetTagColumnName(this.txtNewBalance, null);
            this.dcm1.SetTextColumnName(this.txtNewBalance, "");
            this.txtNewBalance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNewBalance_KeyDown);
            // 
            // txtConsumeType
            // 
            this.comboxList1.SetColumns(this.txtConsumeType, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtConsumeType, 0);
            this.txtConsumeType.EmptyTextTip = "回车选择账目类型";
            this.txtConsumeType.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtConsumeType, 0);
            this.dcm1.SetIsUse(this.txtConsumeType, false);
            this.comboxList1.SetIsUse(this.txtConsumeType, false);
            this.txtConsumeType.Location = new System.Drawing.Point(101, 111);
            this.txtConsumeType.Name = "txtConsumeType";
            this.comboxList1.SetNextControl(this.txtConsumeType, this.btnOK);
            this.txtConsumeType.Size = new System.Drawing.Size(246, 21);
            this.txtConsumeType.TabIndex = 30;
            this.dcm1.SetTagColumnName(this.txtConsumeType, null);
            this.dcm1.SetTextColumnName(this.txtConsumeType, "");
            this.txtConsumeType.TextChanged += new System.EventHandler(this.txtConsumeType_TextChanged);
            // 
            // txtAccount
            // 
            this.comboxList1.SetColumns(this.txtAccount, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtAccount, 0);
            this.txtAccount.EmptyTextTip = "回车选择账户";
            this.txtAccount.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtAccount, 0);
            this.dcm1.SetIsUse(this.txtAccount, true);
            this.comboxList1.SetIsUse(this.txtAccount, false);
            this.txtAccount.Location = new System.Drawing.Point(101, 22);
            this.txtAccount.Name = "txtAccount";
            this.comboxList1.SetNextControl(this.txtAccount, this.txtNewBalance);
            this.txtAccount.Size = new System.Drawing.Size(246, 21);
            this.txtAccount.TabIndex = 0;
            this.dcm1.SetTagColumnName(this.txtAccount, "Id");
            this.dcm1.SetTextColumnName(this.txtAccount, "ActName");
            this.txtAccount.TextChanged += new System.EventHandler(this.txtConsumeType_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 12);
            this.label6.TabIndex = 29;
            this.label6.Text = "账目类型:";
            // 
            // comboxList1
            // 
            this.comboxList1.GridViewDataSource = null;
            this.comboxList1.MaxRowCount = 5;
            this.comboxList1.SelectedRowHandle += new ZhCun.Framework.WinCommon.Components.ComboxListSelectedRowAfterDeleate(this.comboxList1_SelectedRowHandle);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(60, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 12);
            this.label7.TabIndex = 29;
            this.label7.Text = "账户:";
            // 
            // FrmZhangHuDuiZhang
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(403, 287);
            this.Controls.Add(this.txtAccount);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtConsumeType);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtNewBalance);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtBalance);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtRemark);
            this.Controls.Add(this.txtActNamePY);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmZhangHuDuiZhang";
            this.Text = "账户对账";
            this.Load += new System.EventHandler(this.FrmZhangHuDuiZhang_Load);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.txtActNamePY, 0);
            this.Controls.SetChildIndex(this.txtRemark, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnClose, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.txtBalance, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.txtNewBalance, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.txtConsumeType, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.txtAccount, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TextBox txtRemark;
        private System.Windows.Forms.TextBox txtActNamePY;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBalance;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNewBalance;
        private ZhCun.Framework.WinCommon.Components.DCM dcm1;
        private System.Windows.Forms.Label label6;
        private ZhCun.Framework.WinCommon.Controls.ZCTextBox txtConsumeType;
        private ZhCun.Framework.WinCommon.Components.ComboxList comboxList1;
        private System.Windows.Forms.Label label7;
        private ZhCun.Framework.WinCommon.Controls.ZCTextBox txtAccount;
    }
}