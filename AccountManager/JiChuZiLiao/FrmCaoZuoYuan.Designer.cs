﻿namespace AccountManager.JiChuZiLiao
{
    partial class FrmCaoZuoYuan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripTop2 = new System.Windows.Forms.ToolStrip();
            this.tsTxtQuickSearch = new System.Windows.Forms.ToolStripTextBox();
            this.tsBtnQuickSearch = new System.Windows.Forms.ToolStripButton();
            this.tsBtnQuickSearchClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripTop1 = new System.Windows.Forms.ToolStrip();
            this.tsBtnAdd = new System.Windows.Forms.ToolStripButton();
            this.tsBtnEditor = new System.Windows.Forms.ToolStripButton();
            this.tsBtnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsBtnClose = new System.Windows.Forms.ToolStripButton();
            this.ucPagingNavigation = new ZhCun.Framework.WinCommon.Controls.UCPagingNavigation();
            this.dgv = new ZhCun.Framework.WinCommon.Controls.ZCDataGridView();
            this.col_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_NamePY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_LoginName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_LastTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_AddTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStripTop2.SuspendLayout();
            this.toolStripTop1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripTop2
            // 
            this.toolStripTop2.AutoSize = false;
            this.toolStripTop2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripTop2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsTxtQuickSearch,
            this.tsBtnQuickSearch,
            this.tsBtnQuickSearchClear});
            this.toolStripTop2.Location = new System.Drawing.Point(0, 38);
            this.toolStripTop2.Name = "toolStripTop2";
            this.toolStripTop2.Size = new System.Drawing.Size(744, 38);
            this.toolStripTop2.TabIndex = 14;
            this.toolStripTop2.Text = "toolStrip3";
            // 
            // tsTxtQuickSearch
            // 
            this.tsTxtQuickSearch.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsTxtQuickSearch.Margin = new System.Windows.Forms.Padding(5);
            this.tsTxtQuickSearch.Name = "tsTxtQuickSearch";
            this.tsTxtQuickSearch.Size = new System.Drawing.Size(148, 28);
            this.tsTxtQuickSearch.ToolTipText = "输入回车进行快速检索";
            this.tsTxtQuickSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tsTxtQuickSearch_KeyDown);
            // 
            // tsBtnQuickSearch
            // 
            this.tsBtnQuickSearch.Checked = true;
            this.tsBtnQuickSearch.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsBtnQuickSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtnQuickSearch.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnQuickSearch.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnQuickSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnQuickSearch.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.tsBtnQuickSearch.Name = "tsBtnQuickSearch";
            this.tsBtnQuickSearch.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.tsBtnQuickSearch.Size = new System.Drawing.Size(61, 35);
            this.tsBtnQuickSearch.Text = "检索";
            this.tsBtnQuickSearch.Click += new System.EventHandler(this.tsBtnQuickSearch_Click);
            // 
            // tsBtnQuickSearchClear
            // 
            this.tsBtnQuickSearchClear.Checked = true;
            this.tsBtnQuickSearchClear.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsBtnQuickSearchClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtnQuickSearchClear.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnQuickSearchClear.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnQuickSearchClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnQuickSearchClear.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.tsBtnQuickSearchClear.Name = "tsBtnQuickSearchClear";
            this.tsBtnQuickSearchClear.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.tsBtnQuickSearchClear.Size = new System.Drawing.Size(61, 35);
            this.tsBtnQuickSearchClear.Text = "清空";
            this.tsBtnQuickSearchClear.Click += new System.EventHandler(this.tsBtnQuickSearchClear_Click);
            // 
            // toolStripTop1
            // 
            this.toolStripTop1.AutoSize = false;
            this.toolStripTop1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripTop1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnAdd,
            this.tsBtnEditor,
            this.tsBtnDelete,
            this.toolStripSeparator5,
            this.tsBtnClose});
            this.toolStripTop1.Location = new System.Drawing.Point(0, 0);
            this.toolStripTop1.Name = "toolStripTop1";
            this.toolStripTop1.Size = new System.Drawing.Size(744, 38);
            this.toolStripTop1.TabIndex = 13;
            this.toolStripTop1.Text = "toolStrip2";
            // 
            // tsBtnAdd
            // 
            this.tsBtnAdd.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnAdd.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnAdd.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnAdd.Image = global::AccountManager.Properties.Resources.add;
            this.tsBtnAdd.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnAdd.Name = "tsBtnAdd";
            this.tsBtnAdd.Size = new System.Drawing.Size(73, 35);
            this.tsBtnAdd.Text = "增加";
            this.tsBtnAdd.Click += new System.EventHandler(this.tsBtnAdd_Click);
            // 
            // tsBtnEditor
            // 
            this.tsBtnEditor.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnEditor.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnEditor.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnEditor.Image = global::AccountManager.Properties.Resources.edit;
            this.tsBtnEditor.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnEditor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnEditor.Name = "tsBtnEditor";
            this.tsBtnEditor.Size = new System.Drawing.Size(73, 35);
            this.tsBtnEditor.Text = "编辑";
            this.tsBtnEditor.Click += new System.EventHandler(this.tsBtnEditor_Click);
            // 
            // tsBtnDelete
            // 
            this.tsBtnDelete.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnDelete.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnDelete.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnDelete.Image = global::AccountManager.Properties.Resources.delete;
            this.tsBtnDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnDelete.Name = "tsBtnDelete";
            this.tsBtnDelete.Size = new System.Drawing.Size(73, 35);
            this.tsBtnDelete.Text = "删除";
            this.tsBtnDelete.Click += new System.EventHandler(this.tsBtnDelete_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 38);
            // 
            // tsBtnClose
            // 
            this.tsBtnClose.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsBtnClose.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnClose.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnClose.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnClose.Image = global::AccountManager.Properties.Resources.exit;
            this.tsBtnClose.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnClose.Margin = new System.Windows.Forms.Padding(0, 1, 1, 2);
            this.tsBtnClose.Name = "tsBtnClose";
            this.tsBtnClose.Size = new System.Drawing.Size(73, 35);
            this.tsBtnClose.Text = "退出";
            this.tsBtnClose.Click += new System.EventHandler(this.tsBtnClose_Click);
            // 
            // ucPagingNavigation
            // 
            this.ucPagingNavigation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ucPagingNavigation.Location = new System.Drawing.Point(0, 467);
            this.ucPagingNavigation.Name = "ucPagingNavigation";
            this.ucPagingNavigation.Size = new System.Drawing.Size(744, 36);
            this.ucPagingNavigation.TabIndex = 19;
            this.ucPagingNavigation.PageChangedEvent += new ZhCun.Framework.WinCommon.Controls.PadeChangedHandle(this.ucPagingNavigation_PageChangedEvent);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.Snow;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_Name,
            this.col_NamePY,
            this.col_LoginName,
            this.col_LastTime,
            this.col_AddTime,
            this.col_Id});
            this.dgv.DisplayRowCount = true;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(0, 76);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgv.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.RowTemplate.Height = 23;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(744, 391);
            this.dgv.TabIndex = 20;
            this.dgv.UseControlStyle = true;
            // 
            // col_Name
            // 
            this.col_Name.DataPropertyName = "Name";
            this.col_Name.HeaderText = "操作员名";
            this.col_Name.Name = "col_Name";
            this.col_Name.ReadOnly = true;
            // 
            // col_NamePY
            // 
            this.col_NamePY.DataPropertyName = "NamePY";
            this.col_NamePY.HeaderText = "姓名拼音码";
            this.col_NamePY.Name = "col_NamePY";
            this.col_NamePY.ReadOnly = true;
            // 
            // col_LoginName
            // 
            this.col_LoginName.DataPropertyName = "LoginName";
            this.col_LoginName.HeaderText = "登录名";
            this.col_LoginName.Name = "col_LoginName";
            this.col_LoginName.ReadOnly = true;
            // 
            // col_LastTime
            // 
            this.col_LastTime.DataPropertyName = "LastTime";
            this.col_LastTime.HeaderText = "更新时间";
            this.col_LastTime.Name = "col_LastTime";
            this.col_LastTime.ReadOnly = true;
            // 
            // col_AddTime
            // 
            this.col_AddTime.DataPropertyName = "AddTime";
            this.col_AddTime.HeaderText = "增加时间";
            this.col_AddTime.Name = "col_AddTime";
            this.col_AddTime.ReadOnly = true;
            // 
            // col_Id
            // 
            this.col_Id.DataPropertyName = "Id";
            this.col_Id.HeaderText = "Id";
            this.col_Id.Name = "col_Id";
            this.col_Id.ReadOnly = true;
            this.col_Id.Visible = false;
            // 
            // FrmCaoZuoYuan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 503);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.ucPagingNavigation);
            this.Controls.Add(this.toolStripTop2);
            this.Controls.Add(this.toolStripTop1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCaoZuoYuan";
            this.Text = "操作员管理";
            this.Load += new System.EventHandler(this.FrmCaoZuoYuan_Load);
            this.Controls.SetChildIndex(this.toolStripTop1, 0);
            this.Controls.SetChildIndex(this.toolStripTop2, 0);
            this.Controls.SetChildIndex(this.ucPagingNavigation, 0);
            this.Controls.SetChildIndex(this.dgv, 0);
            this.toolStripTop2.ResumeLayout(false);
            this.toolStripTop2.PerformLayout();
            this.toolStripTop1.ResumeLayout(false);
            this.toolStripTop1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ToolStrip toolStripTop2;
        public System.Windows.Forms.ToolStripTextBox tsTxtQuickSearch;
        public System.Windows.Forms.ToolStripButton tsBtnQuickSearch;
        public System.Windows.Forms.ToolStripButton tsBtnQuickSearchClear;
        public System.Windows.Forms.ToolStrip toolStripTop1;
        public System.Windows.Forms.ToolStripButton tsBtnAdd;
        public System.Windows.Forms.ToolStripButton tsBtnEditor;
        public System.Windows.Forms.ToolStripButton tsBtnDelete;
        public System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        public System.Windows.Forms.ToolStripButton tsBtnClose;
        private ZhCun.Framework.WinCommon.Controls.UCPagingNavigation ucPagingNavigation;
        private ZhCun.Framework.WinCommon.Controls.ZCDataGridView dgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_NamePY;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_LoginName;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_LastTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_AddTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Id;
    }
}