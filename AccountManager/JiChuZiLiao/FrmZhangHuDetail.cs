﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;
using AccountManager.TableModel.Model;
using System.Data.Common;
using System.Data.SqlClient;

namespace AccountManager.JiChuZiLiao
{
    public partial class FrmZhangHuDetail : BaseForm
    {
        public FrmZhangHuDetail(string accountId)
        {
            InitializeComponent();
            this._accountId = accountId;
        }
        ALZhangHuDetail _ALObj;
        ALZhangHuDetail.ConsumeSearchModel _SearchModel;
        string _accountId;

        private void FrmZhangHuDetail_Load(object sender, EventArgs e)
        {
            _ALObj = new ALZhangHuDetail();
            _SearchModel = new ALZhangHuDetail.ConsumeSearchModel();
            _SearchModel.Account = _accountId;
            dtpStartDate.Value = DateTime.Now.AddMonths(-1);
            dtpEndDate.Value = DateTime.Now;
            btnSearch_Click(sender, e);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            dcm.SetValueToClassObj(_SearchModel);
            List<VConsume> rList = _ALObj.GetConsumeData(_SearchModel);
            decimal sumAmount = 0;
            if (rList != null)
            {
                foreach (var item in rList)
                {
                    sumAmount += item.Amount;
                }
            }
            dgv.DataSource = rList;
            tslbSumAmount.Text = sumAmount.ToString();
        }
    }
}