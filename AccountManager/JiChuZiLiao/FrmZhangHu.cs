﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;
using ZhCun.Framework.Common.Models;
using AccountManager.TableModel.Model;

namespace AccountManager.JiChuZiLiao
{
    public partial class FrmZhangHu : BaseForm
    {
        public FrmZhangHu()
        {
            InitializeComponent();
        }

        ALZhangHu _ALObj;
        ISearch _SerObj;

        void BindDataSource()
        {
            _SerObj.Clear();
            if (tsTxtQuickSearch.Text.Trim() != string.Empty)
            {
                _SerObj.Add(TAccount.CNActName).LikeFull(tsTxtQuickSearch.Text)
                    .Or(TAccount.CNActNamePY).LikeFull(tsTxtQuickSearch.Text);
            }
            _SerObj.OrderByDesc(TAccount.CNUseCount);
            _SerObj.OnePage = ucPagingNavigation.OnePageCount;
            _SerObj.PageNo = 1;
            List<TAccount> rList = _ALObj.GetAccountData(_SerObj);
            dgv.DataSource = rList;
            decimal sumAmount = 0;
            if (rList != null)
            {
                foreach (var item in rList)
                {
                    sumAmount += item.Balance;
                }
            }
            tslbSumAmount.Text = sumAmount.ToString();
            ucPagingNavigation.InitiControl(_SerObj.RecordCount);
        }

        private void FrmZhangHu_Load(object sender, EventArgs e)
        {
            _ALObj = new ALZhangHu();
            _SerObj = _ALObj.CreateSearch();
            BindDataSource();
        }

        private void tsBtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsBtnQuickSearch_Click(object sender, EventArgs e)
        {
            BindDataSource();
        }

        private void tsBtnQuickSearchClear_Click(object sender, EventArgs e)
        {
            tsTxtQuickSearch.Clear();
        }
        private void tsBtnAdd_Click(object sender, EventArgs e)
        {
            FrmZhangHuEdit frm = new FrmZhangHuEdit(_ALObj.AddAccount);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BindDataSource();
            }
        }
        private void tsBtnEditor_Click(object sender, EventArgs e)
        {
            TAccount selModel = dgv.GetSelectedClassData<TAccount>();
            if (selModel == null)
            {
                ShowMessage("没有选中任何行!");
                return;
            }
            FrmZhangHuEdit frm = new FrmZhangHuEdit(_ALObj.UpdateAccount, selModel);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BindDataSource();
            }
        }
        private void tsBtnDelete_Click(object sender, EventArgs e)
        {
            TAccount selModel = dgv.GetSelectedClassData<TAccount>();
            if (selModel == null)
            {
                ShowMessage("没有选中任何行!");
                return;
            }
            if (ShowQuestion("确实要删除当前选中记录吗?", "删除确认"))
            {
                string errMsg;
                bool r = _ALObj.DeleteAccount(selModel, out errMsg);
                if (!r)
                {
                    ShowMessage(errMsg);
                }
                else
                {
                    BindDataSource();
                }
            }
        }
        private void tsTxtQuickSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BindDataSource();
            }
        }
        private void tsbtnViewDetail_Click(object sender, EventArgs e)
        {
            TAccount selModel = dgv.GetSelectedClassData<TAccount>();
            if (selModel == null)
            {
                ShowMessage("请选择对账记录!!");
                return;
            }
            FrmZhangHuDetail frm = new FrmZhangHuDetail(selModel.Id);
            frm.ShowDialog();
        }
        private void tsBtnDuiZhang_Click(object sender, EventArgs e)
        {
            TAccount selModel = dgv.GetSelectedClassData<TAccount>();
            if (selModel == null)
            {
                ShowMessage("请选择对账记录!!");
                return;
            }
            FrmZhangHuDuiZhang frm = new FrmZhangHuDuiZhang(selModel);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BindDataSource();
            }
        }
        private void tsBtnZhuanZhang_Click(object sender, EventArgs e)
        {
            TAccount selModel = dgv.GetSelectedClassData<TAccount>();
            if (selModel == null)
            {
                ShowMessage("请选择对账记录!!");
                return;
            }
            FrmZhuangZhang frm = new FrmZhuangZhang(selModel);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BindDataSource();
            }
        }
        private void ucPagingNavigation_PageChangedEvent(int pageNo, int onePageCount, out int recordCount)
        {
            _SerObj.OnePage = onePageCount;
            _SerObj.PageNo = pageNo;
            List<TAccount> rList = _ALObj.GetAccountData(_SerObj);
            dgv.DataSource = rList;
            recordCount = _SerObj.RecordCount;
        }
    }
}