﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;
using AccountManager.TableModel.Model;

namespace AccountManager.JiChuZiLiao
{
    public partial class FrmZhangMuLeiXing_old : BaseForm
    {
        public FrmZhangMuLeiXing_old()
        {
            InitializeComponent();
        }

        ALZhangMuLeiXing _ALObj;
        TConsumeType _SelectedConsumeType;

        public TConsumeType SelectedConsumeType
        {
            get
            {
                return _SelectedConsumeType;
            }
        }

        //void BindDataSource()
        //{
        //    tv.Nodes.Clear();
        //    List<TConsumeType> rList = _ALObj.GetConsumeTypeData();
        //    if (rList == null) return;
        //    foreach (var item in rList)
        //    {
        //        if (string.IsNullOrEmpty(item.PId))
        //        {
        //            TreeNode tn = new TreeNode();
        //            tn.Text = item.TypeName;
        //            tn.Tag = item;
        //            tv.Nodes.Add(tn);
        //            LoadTreeData(rList, tn);
        //        }
        //    }
        //    tv.ExpandAll();
        //}
        //void LoadTreeData(List<TConsumeType> typeList, TreeNode pTn)
        //{
        //    TConsumeType cType = pTn.Tag as TConsumeType;
        //    foreach (var item in typeList)
        //    {
        //        if (item.PId == cType.Id)
        //        {
        //            TreeNode tn = new TreeNode();
        //            tn.Text = item.TypeName;
        //            tn.Tag = item;
        //            pTn.Nodes.Add(tn);
        //            LoadTreeData(typeList, tn);
        //        }
        //    }
        //}

        private void FrmZhangMuLeiXing_Load(object sender, EventArgs e)
        {
           // _ALObj = new ALZhangMuLeiXing();
            //BindDataSource();
            

        }

        private void tsBtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void tsBtnAddRoot_Click(object sender, EventArgs e)
        {
            FrmZhangMuLeiXingEdit frm = new FrmZhangMuLeiXingEdit(_ALObj.AddConsumeType, null);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //BindDataSource();
            }
        }
        private void tsBtnAdd_Click(object sender, EventArgs e)
        {
            if (tv.SelectedNode == null)
            {
                ShowMessage("请选中一个节点!");
                return;
            }
            TConsumeType model = new TConsumeType();
            TConsumeType selModel = tv.SelectedNode.Tag as TConsumeType;
            //model.PId = selModel.Id;
            //model.LevelIndex = selModel.LevelIndex + 1;
            //FrmZhangMuLeiXingEdit frm = new FrmZhangMuLeiXingEdit(_ALObj.AddConsumeType, model);
            //if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
            //    //BindDataSource();
            //}
        }

        private void tsBtnEditor_Click(object sender, EventArgs e)
        {
            if (tv.SelectedNode == null)
            {
                ShowMessage("请选中一个节点!");
                return;
            }
            TConsumeType selModel = tv.SelectedNode.Tag as TConsumeType;
            //FrmZhangMuLeiXingEdit frm = new FrmZhangMuLeiXingEdit(_ALObj.UpdateConsumeType, selModel);
            //if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
            //    //BindDataSource();
            //}
        }

        private void tsBtnDelete_Click(object sender, EventArgs e)
        {

            if (tv.SelectedNode == null)
            {
                ShowMessage("请选中一个节点!");
                return;
            }
            //TConsumeType selModel = tv.SelectedNode.Tag as TConsumeType;
            //if (!ShowQuestion("确实要删除 {0} 吗?", "删除确认", selModel.TypeName))
            //{
            //    return;
            //}
            //string errMsg;
            //bool r = _ALObj.DeleteConsumeType(selModel, out errMsg);
            //if (!r)
            //{
            //    ShowMessage(errMsg);
            //}
            //else
            //{
            //    //BindDataSource();
            //}
        }

        private void tsBtnSelected_Click(object sender, EventArgs e)
        {
            if (tv.SelectedNode == null)
            {
                ShowMessage("没有选中任何记录!");
            }
            else
            {

                _SelectedConsumeType = tv.SelectedNode.Tag as TConsumeType;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        #region 拖拽操作


        private void tv_ItemDrag(object sender, ItemDragEventArgs e)
        {
            DoDragDrop(e.Item, DragDropEffects.Move);
        }

        private void tv_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(TreeNode)))
                e.Effect = DragDropEffects.Move;
            else
                e.Effect = DragDropEffects.None;
        }

        private Point Position = new Point(0, 0);

        private void tv_DragDrop(object sender, DragEventArgs e)
        {
            TreeNode myNode = null;
            if (e.Data.GetDataPresent(typeof(TreeNode)))
            {
                myNode = (TreeNode)(e.Data.GetData(typeof(TreeNode)));
            }
            else
            {
                MessageBox.Show("error");
            }
            Position.X = e.X;
            Position.Y = e.Y;
            Position = tv.PointToClient(Position);
            TreeNode dropNode = this.tv.GetNodeAt(Position);
            // 1.目标节点不是空。2.目标节点不是被拖拽接点的字节点。3.目标节点不是被拖拽节点本身  
            if (dropNode != null && dropNode.Parent != myNode && dropNode != myNode)
            {
                TreeNode DragNode = myNode;
                // 将被拖拽节点从原来位置删除。  
                myNode.Remove();
                // 在目标节点下增加被拖拽节点  
                dropNode.Nodes.Add(DragNode);
                //TConsumeType sourceType = myNode.Tag as TConsumeType;
                //TConsumeType newType = dropNode.Tag as TConsumeType;
                //sourceType.LevelIndex = DragNode.Level;
                //_ALObj.UpdatePId(sourceType, newType);
            }
            // 如果目标节点不存在，即拖拽的位置不存在节点，那么就将被拖拽节点放在根节点之下  
            if (dropNode == null)
            {
                TreeNode DragNode = myNode;
                myNode.Remove();
                tv.Nodes.Add(DragNode);
                //TConsumeType sourceType = DragNode.Tag as TConsumeType;
                //sourceType.LevelIndex = 0;
                //_ALObj.UpdatePId(sourceType, null);
            }
        }

        #endregion

        private void tv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                tsBtnSelected_Click(sender, e);
            }
        }
    }
}