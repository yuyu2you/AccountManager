﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;
using AccountManager.JiChuZiLiao;
using AccountManager.TableModel.Model;
using AccountManager.AL.Enums;
using ZhCun.Framework.Common;

namespace AccountManager
{
    public partial class FrmJiZhangView : BaseForm
    {
        public FrmJiZhangView()
        {
            InitializeComponent();
        }

        ALJiZhangView _ALObj;
        ALJiZhangView.ConsumeSearchModel _SearchModel;

        void BindDataSource()
        {
            dcm1.SetValueToClassObj(_SearchModel);
            _SearchModel.IsAddTime = rdoAddTime.Checked;
            var rList = _ALObj.GetConsumeData(_SearchModel);
            dgv.DataSource = rList;
        }

        private void FrmJiZhangView_Load(object sender, EventArgs e)
        {
            _ALObj = new ALJiZhangView();
            _SearchModel = new ALJiZhangView.ConsumeSearchModel();
            coBoxBalanceType.Items.Clear();
            coBoxBalanceType.Items.Add(new { Id = 0, Caption = "全部" });
            coBoxBalanceType.Items.Add(new { Id = (int)EnBalanceType.ZhiChu, Caption = "支出" });
            coBoxBalanceType.Items.Add(new { Id = (int)EnBalanceType.ShouRu, Caption = "收入" });
            coBoxBalanceType.DisplayMember = "Caption";
            coBoxBalanceType.ValueMember = "Id";
            coBoxBalanceType.SelectedIndex = 0;
            ComboxListHelper.SetConsumeType(comboxList1, txtConsumeType, EnConsumeTypeCategory.None);
            ComboxListHelper.SetAccount(comboxList1, txtAccount);
            dtpStartDate.Value = DateTime.Now.AddDays(-3);
            dtpEndDate.Value = DateTime.Now;
            BindDataSource();
        }

        private void tsBtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsBtnOutBalance_Click(object sender, EventArgs e)
        {
            FrmJiZhang frm = new FrmJiZhang(_ALObj.AddOutConsume, EnBalanceType.ZhiChu);
            frm.Text = "支出记账";
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BindDataSource();
            }
        }

        private void tsBtnInBalance_Click(object sender, EventArgs e)
        {
            FrmJiZhang frm = new FrmJiZhang(_ALObj.AddInConsume, EnBalanceType.ShouRu);
            frm.Text = "收入记账";
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BindDataSource();
            }
        }

        private void tsBtnCopyJiZhang_Click(object sender, EventArgs e)
        {
            VConsume selModel = dgv.GetSelectedClassData<VConsume>();
            if (selModel == null)
            {
                ShowMessage("请选择要冲账的记录!");
                return;
            }
            if (selModel.IsCancel)
            {
                ShowMessage("该流水已冲账");
                return;
            }
            EnBalanceType balanceType = (EnBalanceType)selModel.BalanceTypeId;
            FrmJiZhang frm = null;
            switch (balanceType)
            {
                case EnBalanceType.ZhiChu:
                    frm = new FrmJiZhang(_ALObj.AddOutConsume, EnBalanceType.ZhiChu, selModel, true);
                    frm.Text = "支出记账-复制";
                    break;
                case EnBalanceType.ShouRu:
                    frm = new FrmJiZhang(_ALObj.AddInConsume, EnBalanceType.ShouRu, selModel, true);
                    frm.Text = "收入记账-复制";
                    break;
                case EnBalanceType.ZhiChuChongZhang:
                case EnBalanceType.ShouRuChongZhang:
                case EnBalanceType.DuizhangZhiChu:
                case EnBalanceType.DuiZhangShouRu:
                default:
                    ShowMessage("类型有误或程序逻辑bug!");
                    return;
            }
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BindDataSource();
            }
        }

        private void btnChongZhang_Click(object sender, EventArgs e)
        {
            VConsume selModel = dgv.GetSelectedClassData<VConsume>();
            if (selModel == null)
            {
                ShowMessage("请选择要冲账的记录!");
                return;
            }
            if (selModel.IsCancel)
            {
                ShowMessage("该流水已冲账");
                return;
            }
            if (!ShowQuestion("确实要对选中记录冲账吗?该操作不可恢复", "冲账确认"))
            {
                return;
            }
            string errMsg;
            bool r = _ALObj.ChongZhang(selModel, out errMsg);
            if (!r)
            {
                ShowMessage(errMsg);
            }
            else
            {
                ShowMessage("冲账成功!");
                BindDataSource();
            }
        }

        private void btnConsumeEdit_Click(object sender, EventArgs e)
        {
            VConsume selModel = dgv.GetSelectedClassData<VConsume>();
            if (selModel == null)
            {
                ShowMessage("请选择要冲账的记录!");
                return;
            }
            if (selModel.IsCancel)
            {
                ShowMessage("已冲账就不要编辑了!~");
                return;
            }
            EnBalanceType balanctType = (EnBalanceType)selModel.BalanceTypeId;
            FrmJiZhang frm = new FrmJiZhang(_ALObj.EditConsume, balanctType, selModel);
            frm.Text = "流水编辑";
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BindDataSource();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindDataSource();
        }

        private void txtConsumeType_TextChanged(object sender, EventArgs e)
        {
            if (txtConsumeType.Text == string.Empty)
            {
                txtConsumeType.Tag = null;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            dtpEndDate.Value = _ALObj.GetNowTime();
            dtpStartDate.Value = _ALObj.GetNowTime();
            txtAccount.Clear();
            txtAccount.Tag = null;
            txtConsumeName.Clear();
            txtConsumeName.Tag = null;
            txtConsumeType.Clear();
            txtConsumeType.Tag = null;
            coBoxBalanceType.SelectedIndex = 0;
        }

        private void tsBtnUndoLastAccount_Click(object sender, EventArgs e)
        {
            if (ShowQuestion("确实撤销当天最后一笔收支记录吗?此操作不可恢复","撤销记账确认"))
            { 
                string errMsg;
                bool r = _ALObj.UndoLastAccount(out errMsg);
                if (!r)
                {
                    ShowMessage(errMsg);
                }
                else
                {
                    ShowMessage("撤销成功!");
                    BindDataSource();
                }
            }
        }
    }
}