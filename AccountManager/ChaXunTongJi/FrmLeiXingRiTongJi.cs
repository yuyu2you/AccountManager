﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;

namespace AccountManager.ChaXunTongJi
{
    public partial class FrmLeiXingRiTongJi : BaseForm
    {
        public FrmLeiXingRiTongJi()
        {
            InitializeComponent();
        }
        ALLeiXingRiTongJi _ALObj;
        ALLeiXingRiTongJi.SearchModel _SearchModel;

        private void FrmLeiXingRiTongJi_Load(object sender, EventArgs e)
        {
            _ALObj = new ALLeiXingRiTongJi();
            _SearchModel = new ALLeiXingRiTongJi.SearchModel();
            dgv.AutoGenerateColumns = true;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            dcm1.SetValueToClassObj(_SearchModel);
            decimal sumAmount;
            DataTable dt = _ALObj.GetLeiXingRiTongJi(_SearchModel, out sumAmount);
            dgv.DataSource = dt;
            tslbRecordCount.Text = dt.Rows.Count.ToString();
            tslbSumAmount.Text = sumAmount.ToString();
        }
    }
}