﻿namespace AccountManager.ChaXunTongJi
{
    partial class FrmZongHeYeWuMingXi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.coBoxIsCancel = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chBoxIsAddTime = new System.Windows.Forms.CheckBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnAdwSearch = new System.Windows.Forms.Button();
            this.txtAccount = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtConsumeType = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.coBoxBalanceType = new System.Windows.Forms.ComboBox();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtConsumeName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dcm = new ZhCun.Framework.WinCommon.Components.DCM(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbRecordCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbSumAmount = new System.Windows.Forms.ToolStripStatusLabel();
            this.dgv = new ZhCun.Framework.WinCommon.Controls.ZCDataGridView();
            this.col_ConsumeDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_ConsumeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_AccountName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_BalanceType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_ConsumeType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Balance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_AddTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_IsCancel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.coBoxIsCancel);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.chBoxIsAddTime);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnAdwSearch);
            this.groupBox1.Controls.Add(this.txtAccount);
            this.groupBox1.Controls.Add(this.btnClear);
            this.groupBox1.Controls.Add(this.txtConsumeType);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.dtpStartDate);
            this.groupBox1.Controls.Add(this.coBoxBalanceType);
            this.groupBox1.Controls.Add(this.dtpEndDate);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtConsumeName);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(998, 125);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查询条件";
            // 
            // coBoxIsCancel
            // 
            this.coBoxIsCancel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coBoxIsCancel.FormattingEnabled = true;
            this.dcm.SetIsUse(this.coBoxIsCancel, false);
            this.coBoxIsCancel.Items.AddRange(new object[] {
            "全部",
            "已冲账",
            "未冲账"});
            this.coBoxIsCancel.Location = new System.Drawing.Point(513, 53);
            this.coBoxIsCancel.Name = "coBoxIsCancel";
            this.coBoxIsCancel.Size = new System.Drawing.Size(152, 20);
            this.coBoxIsCancel.TabIndex = 16;
            this.dcm.SetTagColumnName(this.coBoxIsCancel, null);
            this.dcm.SetTextColumnName(this.coBoxIsCancel, null);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(451, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 12);
            this.label7.TabIndex = 15;
            this.label7.Text = "冲账标志:";
            // 
            // chBoxIsAddTime
            // 
            this.chBoxIsAddTime.AutoSize = true;
            this.chBoxIsAddTime.Checked = true;
            this.chBoxIsAddTime.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dcm.SetIsUse(this.chBoxIsAddTime, true);
            this.chBoxIsAddTime.Location = new System.Drawing.Point(78, 25);
            this.chBoxIsAddTime.Name = "chBoxIsAddTime";
            this.chBoxIsAddTime.Size = new System.Drawing.Size(108, 16);
            this.chBoxIsAddTime.TabIndex = 14;
            this.dcm.SetTagColumnName(this.chBoxIsAddTime, null);
            this.chBoxIsAddTime.Text = "按记账时间查询";
            this.dcm.SetTextColumnName(this.chBoxIsAddTime, "IsAddTime");
            this.chBoxIsAddTime.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(794, 69);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 25);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "退出";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnAdwSearch
            // 
            this.btnAdwSearch.Location = new System.Drawing.Point(794, 29);
            this.btnAdwSearch.Name = "btnAdwSearch";
            this.btnAdwSearch.Size = new System.Drawing.Size(75, 25);
            this.btnAdwSearch.TabIndex = 13;
            this.btnAdwSearch.Text = "高级查询";
            this.btnAdwSearch.UseVisualStyleBackColor = true;
            this.btnAdwSearch.Click += new System.EventHandler(this.btnAdwSearch_Click);
            // 
            // txtAccount
            // 
            this.txtAccount.EmptyTextTip = "回车选择账户";
            this.txtAccount.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetIsUse(this.txtAccount, true);
            this.txtAccount.Location = new System.Drawing.Point(513, 22);
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Size = new System.Drawing.Size(152, 21);
            this.txtAccount.TabIndex = 11;
            this.dcm.SetTagColumnName(this.txtAccount, "Account");
            this.dcm.SetTextColumnName(this.txtAccount, null);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(698, 69);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 25);
            this.btnClear.TabIndex = 10;
            this.btnClear.Text = "清空";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // txtConsumeType
            // 
            this.txtConsumeType.EmptyTextTip = "回车选择消费类型";
            this.txtConsumeType.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetIsUse(this.txtConsumeType, true);
            this.txtConsumeType.Location = new System.Drawing.Point(284, 81);
            this.txtConsumeType.Name = "txtConsumeType";
            this.txtConsumeType.Size = new System.Drawing.Size(152, 21);
            this.txtConsumeType.TabIndex = 11;
            this.dcm.SetTagColumnName(this.txtConsumeType, "ConsumeType");
            this.dcm.SetTextColumnName(this.txtConsumeType, null);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "开始日期:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "结束日期:";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(698, 29);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 25);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "查询";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dtpStartDate
            // 
            this.dcm.SetIsUse(this.dtpStartDate, true);
            this.dtpStartDate.Location = new System.Drawing.Point(78, 53);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(133, 21);
            this.dtpStartDate.TabIndex = 1;
            this.dcm.SetTagColumnName(this.dtpStartDate, null);
            this.dcm.SetTextColumnName(this.dtpStartDate, "StartTime");
            // 
            // coBoxBalanceType
            // 
            this.coBoxBalanceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coBoxBalanceType.FormattingEnabled = true;
            this.dcm.SetIsUse(this.coBoxBalanceType, true);
            this.coBoxBalanceType.Location = new System.Drawing.Point(284, 23);
            this.coBoxBalanceType.Name = "coBoxBalanceType";
            this.coBoxBalanceType.Size = new System.Drawing.Size(152, 20);
            this.coBoxBalanceType.TabIndex = 9;
            this.dcm.SetTagColumnName(this.coBoxBalanceType, "BalanceType");
            this.dcm.SetTextColumnName(this.coBoxBalanceType, null);
            // 
            // dtpEndDate
            // 
            this.dcm.SetIsUse(this.dtpEndDate, true);
            this.dtpEndDate.Location = new System.Drawing.Point(78, 84);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(133, 21);
            this.dtpEndDate.TabIndex = 1;
            this.dcm.SetTagColumnName(this.dtpEndDate, null);
            this.dcm.SetTextColumnName(this.dtpEndDate, "EndTime");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(449, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "账户:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(219, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "消费说明:";
            // 
            // txtConsumeName
            // 
            this.dcm.SetIsUse(this.txtConsumeName, true);
            this.txtConsumeName.Location = new System.Drawing.Point(284, 54);
            this.txtConsumeName.Name = "txtConsumeName";
            this.txtConsumeName.Size = new System.Drawing.Size(152, 21);
            this.txtConsumeName.TabIndex = 3;
            this.dcm.SetTagColumnName(this.txtConsumeName, null);
            this.dcm.SetTextColumnName(this.txtConsumeName, "ConsumeName");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(219, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "资金类型:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(219, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "消费类型:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tslbRecordCount,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3,
            this.tslbSumAmount});
            this.statusStrip1.Location = new System.Drawing.Point(0, 610);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(998, 22);
            this.statusStrip1.TabIndex = 25;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(56, 17);
            this.toolStripStatusLabel1.Text = "记录数：";
            // 
            // tslbRecordCount
            // 
            this.tslbRecordCount.BackColor = System.Drawing.Color.Transparent;
            this.tslbRecordCount.ForeColor = System.Drawing.Color.Red;
            this.tslbRecordCount.Name = "tslbRecordCount";
            this.tslbRecordCount.Size = new System.Drawing.Size(20, 17);
            this.tslbRecordCount.Text = "０";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(11, 17);
            this.toolStripStatusLabel2.Text = "|";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(80, 17);
            this.toolStripStatusLabel3.Text = "账户增加额：";
            // 
            // tslbSumAmount
            // 
            this.tslbSumAmount.BackColor = System.Drawing.Color.Transparent;
            this.tslbSumAmount.ForeColor = System.Drawing.Color.Red;
            this.tslbSumAmount.Name = "tslbSumAmount";
            this.tslbSumAmount.Size = new System.Drawing.Size(20, 17);
            this.tslbSumAmount.Text = "０";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.Snow;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_ConsumeDate,
            this.col_ConsumeName,
            this.col_AccountName,
            this.col_BalanceType,
            this.col_ConsumeType,
            this.col_Amount,
            this.col_Balance,
            this.col_AddTime,
            this.col_IsCancel});
            this.dgv.DisplayRowCount = true;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(0, 125);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgv.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.RowTemplate.Height = 23;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(998, 485);
            this.dgv.TabIndex = 26;
            this.dgv.UseControlStyle = true;
            // 
            // col_ConsumeDate
            // 
            this.col_ConsumeDate.DataPropertyName = "ConsumeDate";
            this.col_ConsumeDate.HeaderText = "账目日期";
            this.col_ConsumeDate.Name = "col_ConsumeDate";
            this.col_ConsumeDate.ReadOnly = true;
            // 
            // col_ConsumeName
            // 
            this.col_ConsumeName.DataPropertyName = "ConsumeName";
            this.col_ConsumeName.HeaderText = "账目说明";
            this.col_ConsumeName.Name = "col_ConsumeName";
            this.col_ConsumeName.ReadOnly = true;
            // 
            // col_AccountName
            // 
            this.col_AccountName.DataPropertyName = "AccountName";
            this.col_AccountName.HeaderText = "账户";
            this.col_AccountName.Name = "col_AccountName";
            this.col_AccountName.ReadOnly = true;
            // 
            // col_BalanceType
            // 
            this.col_BalanceType.DataPropertyName = "BalanceTypeName";
            this.col_BalanceType.HeaderText = "资金类型";
            this.col_BalanceType.Name = "col_BalanceType";
            this.col_BalanceType.ReadOnly = true;
            // 
            // col_ConsumeType
            // 
            this.col_ConsumeType.DataPropertyName = "ConsumeTypeName";
            this.col_ConsumeType.HeaderText = "账目类型";
            this.col_ConsumeType.Name = "col_ConsumeType";
            this.col_ConsumeType.ReadOnly = true;
            // 
            // col_Amount
            // 
            this.col_Amount.DataPropertyName = "Amount";
            this.col_Amount.HeaderText = "发生额";
            this.col_Amount.Name = "col_Amount";
            this.col_Amount.ReadOnly = true;
            // 
            // col_Balance
            // 
            this.col_Balance.DataPropertyName = "Balance";
            this.col_Balance.HeaderText = "账户余额";
            this.col_Balance.Name = "col_Balance";
            this.col_Balance.ReadOnly = true;
            // 
            // col_AddTime
            // 
            this.col_AddTime.DataPropertyName = "AddTime";
            this.col_AddTime.HeaderText = "记账日期";
            this.col_AddTime.Name = "col_AddTime";
            this.col_AddTime.ReadOnly = true;
            // 
            // col_IsCancel
            // 
            this.col_IsCancel.DataPropertyName = "IsCancel";
            this.col_IsCancel.HeaderText = "冲账标志";
            this.col_IsCancel.Name = "col_IsCancel";
            this.col_IsCancel.ReadOnly = true;
            // 
            // FrmZongHeYeWuMingXi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 632);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox1);
            this.DockAreas = ((WeifenLuo.WinFormsUI.Docking.DockAreas)((((((WeifenLuo.WinFormsUI.Docking.DockAreas.Float | WeifenLuo.WinFormsUI.Docking.DockAreas.DockLeft)
                        | WeifenLuo.WinFormsUI.Docking.DockAreas.DockRight)
                        | WeifenLuo.WinFormsUI.Docking.DockAreas.DockTop)
                        | WeifenLuo.WinFormsUI.Docking.DockAreas.DockBottom)
                        | WeifenLuo.WinFormsUI.Docking.DockAreas.Document)));
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "FrmZongHeYeWuMingXi";
            this.ShowHint = WeifenLuo.WinFormsUI.Docking.DockState.Unknown;
            this.Text = "综合业务明细查询";
            this.Load += new System.EventHandler(this.FrmZongHeYeWuMingXi_Load);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.statusStrip1, 0);
            this.Controls.SetChildIndex(this.dgv, 0);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private ZhCun.Framework.WinCommon.Controls.ZCTextBox txtAccount;
        private System.Windows.Forms.Button btnClear;
        private ZhCun.Framework.WinCommon.Controls.ZCTextBox txtConsumeType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.ComboBox coBoxBalanceType;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtConsumeName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnAdwSearch;
        private System.Windows.Forms.CheckBox chBoxIsAddTime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox coBoxIsCancel;
        private ZhCun.Framework.WinCommon.Components.DCM dcm;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private ZhCun.Framework.WinCommon.Controls.ZCDataGridView dgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ConsumeDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ConsumeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_AccountName;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_BalanceType;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ConsumeType;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Balance;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_AddTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_IsCancel;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tslbRecordCount;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel tslbSumAmount;

    }
}