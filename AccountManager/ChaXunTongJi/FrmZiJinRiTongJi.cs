﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;

namespace AccountManager.ChaXunTongJi
{
    public partial class FrmZiJinRiTongJi : BaseForm
    {
        public FrmZiJinRiTongJi()
        {
            InitializeComponent();
        }
        ALZiJinRiTongJi _ALObj;
        ALZiJinRiTongJi.SearchModel _SearchModel;

        private void btnSearch_Click(object sender, EventArgs e)
        {
            dcm1.SetValueToClassObj(_SearchModel);
            DataTable dt = _ALObj.GetData(_SearchModel);
            dgv.DataSource = dt;
        }

        private void FrmZiJinRiTongJi_Load(object sender, EventArgs e)
        {
            _ALObj = new ALZiJinRiTongJi();
            _SearchModel = new ALZiJinRiTongJi.SearchModel();
            dgv.AutoGenerateColumns = true;
        }
    }
}
