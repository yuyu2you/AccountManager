﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AccountManager.TableModel.Model;

namespace AccountManager.AL
{
    public class ALLeiXingRiTongJi : ALBase
    {

        public class SearchModel
        {
            public DateTime StartDate { set; get; }
            public DateTime EndDate { set; get; }
            public bool IsAddTime { set; get; }
            public string AccountId { set; get; }
            /// <summary>
            /// 只显示有发生额的内容
            /// </summary>
            public bool OnlyHaveAmount { set; get; }
        }

        public DataTable GetLeiXingRiTongJi(SearchModel serModel, out decimal sumAmount)
        {
            sumAmount = 0;
            Dictionary<string, Dictionary<string, decimal>> consumeGroup = new Dictionary<string, Dictionary<string, decimal>>();

            DataTable rTable = new DataTable();
            rTable.Columns.Add("日期");

            for (DateTime i = serModel.StartDate; i <= serModel.EndDate; i = i.AddDays(1))
            {
                string dateStr = i.ToString("yyyy-MM-dd");
                rTable.Rows.Add(dateStr);
                consumeGroup.Add(dateStr, null);
            }

            CommonSearch.Clear();
            string timeColumnsName = serModel.IsAddTime ? TConsume.CNAddTime : TConsume.CNConsumeDate;
            CommonSearch.Add(timeColumnsName).GreaterThenEqual(serModel.StartDate.ToString("yyyy-MM-dd"));
            CommonSearch.And(timeColumnsName).LessThan(serModel.EndDate.AddDays(1).ToString("yyyy-MM-dd"));
            CommonSearch.OrderByAsc(timeColumnsName);
            if (!string.IsNullOrEmpty(serModel.AccountId))
            {
                CommonSearch.And(TConsume.CNAccountId).Equal(serModel.AccountId);
            }
            List<TConsume> consumeList = DA.GetList<TConsume>(CommonSearch);
            if (consumeList != null)
            {
                DateTime tmpTime;
                foreach (var item in consumeList)
                {
                    tmpTime = timeColumnsName == TConsume.CNAddTime ? item.AddTime : item.ConsumeDate;
                    string dateStr = tmpTime.ToString("yyyy-MM-dd");

                    if (consumeGroup[dateStr] == null)
                    {
                        consumeGroup[dateStr] = new Dictionary<string, decimal>();
                    }
                    if (item.ConsumeTypeId == null) item.ConsumeTypeId = string.Empty;
                    if (!consumeGroup[dateStr].ContainsKey(item.ConsumeTypeId))
                    {
                        consumeGroup[dateStr].Add(item.ConsumeTypeId, 0);
                    }
                    consumeGroup[dateStr][item.ConsumeTypeId] += item.Amount;
                    sumAmount += item.Amount;
                }
            }
            CommonSearch.Clear();
            CommonSearch.OrderByDesc(TConsumeType.CNUseCount);
            List<VConsumeType> typeList = DA.GetList<VConsumeType>(CommonSearch);
            foreach (var item in typeList)
            {
                DataColumn dc = new DataColumn(string.Format("{0}", item.TypeName));
                dc.DefaultValue = "0";
                dc.DataType = typeof(decimal);
                dc.Caption = item.Id;
                rTable.Columns.Add(dc);                
            }
            DataColumn col = new DataColumn("其它(sys)");
            col.DefaultValue = "0";
            col.DataType = typeof(decimal);
            col.Caption = string.Empty;
            rTable.Columns.Add(col);   
            //dt.Columns.Add("其它(空)");
            for (int i = 0; i < rTable.Rows.Count; i++)
            {
                string dateStr = rTable.Rows[i][0].ToString();
                for (int j = 1; j < rTable.Columns.Count; j++)
                {
                    DataColumn dc = rTable.Columns[j];
                    if (consumeGroup[dateStr] != null && consumeGroup[dateStr].ContainsKey(dc.Caption))
                    {
                        rTable.Rows[i][j] = consumeGroup[dateStr][dc.Caption];
                    }
                }
            }
            if (serModel.OnlyHaveAmount)
            {
                bool isNoHaveAmount;
                for (int i = rTable.Columns.Count - 1; i > 0; i--)
                {
                    isNoHaveAmount = true;
                    foreach (var item in consumeGroup.Keys)
                    {
                        if (consumeGroup[item] != null && consumeGroup[item].ContainsKey(rTable.Columns[i].Caption))
                        {
                            isNoHaveAmount = false;
                            continue;
                        }
                    }
                    if (isNoHaveAmount)
                    {
                        rTable.Columns.RemoveAt(i);
                    }
                }
            }
            return rTable;
        }
    }
}