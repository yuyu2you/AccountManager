﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Reflection;
using System.Diagnostics;

namespace AccountManager.Run
{
    public partial class FrmCopyFiles : Form
    {
        public FrmCopyFiles()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }

        /// <summary>
        /// 获得指定路径下的所有子目录
        /// </summary>
        /// <param name="pathList">用来输出的路径列表</param>
        /// <param name="rootPath">要搜索的路径</param>
        static void GetAllDir(List<string> pathList, string rootPath)
        {
            string[] paths = Directory.GetDirectories(rootPath);
            if (paths == null) return;
            foreach (var item in paths)
            {
                pathList.Add(item);
                GetAllDir(pathList, item);
            }
        }
        /// <summary>
        /// 复制指定文件夹下的所有文件到指定文件夹,存在会替换
        /// </summary>
        /// <param name="sourceDir">要复制文件的目录</param>
        /// <param name="targetDir">复制的目标文件夹</param>
        static void CopyDir(string sourceDir, string targetDir)
        {
            //当前应用程序的全路径
            string assemblyFullName = Assembly.GetCallingAssembly().Location;
            List<string> sourcePaths = new List<string>();
            sourcePaths.Add(sourceDir);
            GetAllDir(sourcePaths, sourceDir);
            if (sourcePaths.Count == 0) return;
            if (!string.IsNullOrEmpty(targetDir)) Directory.CreateDirectory(targetDir);
            foreach (var path in sourcePaths)
            {
                if (path != sourceDir)
                {
                    string newTargetDir = targetDir + path.Remove(0, path.IndexOf(sourceDir) + sourceDir.Length + 1);
                    if (!string.IsNullOrEmpty(newTargetDir))
                    {
                        Directory.CreateDirectory(newTargetDir);
                    }
                }
                string[] sourceFiles = Directory.GetFiles(path);
                foreach (var file in sourceFiles)
                {
                    //这种方法而不是替换是为了解决子目录有相同文件夹名字的问题
                    string targetFile = targetDir + file.Remove(0, file.IndexOf(sourceDir) + sourceDir.Length + 1);
                    //如果是自己那么跳过
                    if (targetFile == assemblyFullName || targetFile == Path.GetFileName(assemblyFullName))
                    {
                        //莫名其妙的问题,当把自己改成这个文件的时候,启动不了主程序 2015.1.28
                        //targetFile = targetFile + ".update";
                        targetFile = targetFile + "update"; //改成这个就行
                    }
                    File.Copy(file, targetFile, true);
                    File.Delete(file);
                }
            }
        }

        private void FrmCopyFiles_Load(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(a =>
            {
                Thread.Sleep(1000);
                CopyDir(Program.UpdatePath, "");                
                Directory.Delete(Program.UpdatePath, true);                
                
                this.Close();
            }));
        }

        private void FrmCopyFiles_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}