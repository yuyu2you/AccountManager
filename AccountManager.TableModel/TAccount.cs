using System;
using System.Collections.Generic;
using System.Text;
using ZhCun.Framework.Common.Models.TableModel;

namespace AccountManager.TableModel.Model
{
	public class TAccount : ModelBase
	{
		private System.String _Id;
		/// <summary>
		/// Id
		/// </summary>
		[ModelAttribute(ColumnName = CNId, IsPrimaryKey = true, IsNotNull = true)]
		public System.String Id
		{
			get { return _Id; }
			set { _Id = value; }
		}

		private System.String _ActName;
		/// <summary>
		/// ActName
		/// </summary>
		[ModelAttribute(ColumnName = CNActName)]
		public System.String ActName
		{
			get { return _ActName; }
			set { _ActName = value; }
		}

		private System.String _ActNamePY;
		/// <summary>
		/// ActNamePY
		/// </summary>
		[ModelAttribute(ColumnName = CNActNamePY)]
		public System.String ActNamePY
		{
			get { return _ActNamePY; }
			set { _ActNamePY = value; }
		}

		private System.String _Remark;
		/// <summary>
		/// Remark
		/// </summary>
		[ModelAttribute(ColumnName = CNRemark)]
		public System.String Remark
		{
			get { return _Remark; }
			set { _Remark = value; }
		}

		private System.Decimal _Balance;
		/// <summary>
		/// Balance
		/// </summary>
		[ModelAttribute(ColumnName = CNBalance)]
		public System.Decimal Balance
		{
			get { return _Balance; }
			set { _Balance = value; }
		}

		private System.Int32 _UseCount;
		/// <summary>
		/// UseCount
		/// </summary>
		[ModelAttribute(ColumnName = CNUseCount)]
		public System.Int32 UseCount
		{
			get { return _UseCount; }
			set { _UseCount = value; }
		}

		private System.String _OperatorId;
		/// <summary>
		/// OperatorId
		/// </summary>
		[ModelAttribute(ColumnName = CNOperatorId)]
		public System.String OperatorId
		{
			get { return _OperatorId; }
			set { _OperatorId = value; }
		}

		private System.DateTime _AddTime;
		/// <summary>
		/// AddTime
		/// </summary>
		[ModelAttribute(ColumnName = CNAddTime)]
		public System.DateTime AddTime
		{
			get { return _AddTime; }
			set { _AddTime = value; }
		}

		private System.DateTime _LastTime;
		/// <summary>
		/// LastTime
		/// </summary>
		[ModelAttribute(ColumnName = CNLastTime)]
		public System.DateTime LastTime
		{
			get { return _LastTime; }
			set { _LastTime = value; }
		}

		#region 静态的字段名的方法
		public const string CNId = "Id";
		public const string CNActName = "ActName";
		public const string CNActNamePY = "ActNamePY";
		public const string CNRemark = "Remark";
		public const string CNBalance = "Balance";
		public const string CNUseCount = "UseCount";
		public const string CNOperatorId = "OperatorId";
		public const string CNAddTime = "AddTime";
		public const string CNLastTime = "LastTime";
		#endregion

	}
}
