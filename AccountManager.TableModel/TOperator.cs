using System;
using System.Collections.Generic;
using System.Text;
using ZhCun.Framework.Common.Models.TableModel;

namespace AccountManager.TableModel.Model
{
	public class TOperator : ModelBase
	{
		private System.String _Id;
		/// <summary>
		/// Id
		/// </summary>
		[ModelAttribute(ColumnName = CNId, IsPrimaryKey = true, IsNotNull = true)]
		public System.String Id
		{
			get { return _Id; }
			set { _Id = value; }
		}

		private System.String _Name;
		/// <summary>
		/// Name
		/// </summary>
		[ModelAttribute(ColumnName = CNName)]
		public System.String Name
		{
			get { return _Name; }
			set { _Name = value; }
		}

		private System.String _NamePY;
		/// <summary>
		/// NamePY
		/// </summary>
		[ModelAttribute(ColumnName = CNNamePY)]
		public System.String NamePY
		{
			get { return _NamePY; }
			set { _NamePY = value; }
		}

		private System.String _LoginName;
		/// <summary>
		/// LoginName
		/// </summary>
		[ModelAttribute(ColumnName = CNLoginName)]
		public System.String LoginName
		{
			get { return _LoginName; }
			set { _LoginName = value; }
		}

		private System.String _LoginPwd;
		/// <summary>
		/// LoginPwd
		/// </summary>
		[ModelAttribute(ColumnName = CNLoginPwd)]
		public System.String LoginPwd
		{
			get { return _LoginPwd; }
			set { _LoginPwd = value; }
		}

		private System.DateTime _AddTime;
		/// <summary>
		/// AddTime
		/// </summary>
		[ModelAttribute(ColumnName = CNAddTime)]
		public System.DateTime AddTime
		{
			get { return _AddTime; }
			set { _AddTime = value; }
		}

		private System.DateTime _LastTime;
		/// <summary>
		/// LastTime
		/// </summary>
		[ModelAttribute(ColumnName = CNLastTime)]
		public System.DateTime LastTime
		{
			get { return _LastTime; }
			set { _LastTime = value; }
		}

		private System.String _OperatorId;
		/// <summary>
		/// OperatorId
		/// </summary>
		[ModelAttribute(ColumnName = CNOperatorId)]
		public System.String OperatorId
		{
			get { return _OperatorId; }
			set { _OperatorId = value; }
		}

		private System.Boolean _IsDel;
		/// <summary>
		/// IsDel
		/// </summary>
		[ModelAttribute(ColumnName = CNIsDel)]
		public System.Boolean IsDel
		{
			get { return _IsDel; }
			set { _IsDel = value; }
		}

		#region 静态的字段名的方法
		public const string CNId = "Id";
		public const string CNName = "Name";
		public const string CNNamePY = "NamePY";
		public const string CNLoginName = "LoginName";
		public const string CNLoginPwd = "LoginPwd";
		public const string CNAddTime = "AddTime";
		public const string CNLastTime = "LastTime";
		public const string CNOperatorId = "OperatorId";
		public const string CNIsDel = "IsDel";
		#endregion

	}
}
