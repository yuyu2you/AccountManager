using System;
using System.Collections.Generic;
using System.Text;
using ZhCun.Framework.Common.Models.TableModel;

namespace AccountManager.TableModel.Model
{
	public class VConsume : ModelBase
	{
		private System.String _Id;
		/// <summary>
		/// Id
		/// </summary>
		[ModelAttribute(ColumnName = CNId, IsPrimaryKey = true, IsNotNull = true)]
		public System.String Id
		{
			get { return _Id; }
			set { _Id = value; }
		}

		private System.String _ConsumeName;
		/// <summary>
		/// ConsumeName
		/// </summary>
		[ModelAttribute(ColumnName = CNConsumeName)]
		public System.String ConsumeName
		{
			get { return _ConsumeName; }
			set { _ConsumeName = value; }
		}

		private System.String _ConsumeTypeId;
		/// <summary>
		/// ConsumeTypeId
		/// </summary>
		[ModelAttribute(ColumnName = CNConsumeTypeId)]
		public System.String ConsumeTypeId
		{
			get { return _ConsumeTypeId; }
			set { _ConsumeTypeId = value; }
		}

		private System.String _ConsumeTypeName;
		/// <summary>
		/// ConsumeTypeName
		/// </summary>
		[ModelAttribute(ColumnName = CNConsumeTypeName)]
		public System.String ConsumeTypeName
		{
			get { return _ConsumeTypeName; }
			set { _ConsumeTypeName = value; }
		}

		private System.Int32 _BalanceTypeId;
		/// <summary>
		/// BalanceTypeId
		/// </summary>
		[ModelAttribute(ColumnName = CNBalanceTypeId)]
		public System.Int32 BalanceTypeId
		{
			get { return _BalanceTypeId; }
			set { _BalanceTypeId = value; }
		}

		private System.String _BalanceTypeName;
		/// <summary>
		/// BalanceTypeName
		/// </summary>
		[ModelAttribute(ColumnName = CNBalanceTypeName)]
		public System.String BalanceTypeName
		{
			get { return _BalanceTypeName; }
			set { _BalanceTypeName = value; }
		}

		private System.String _AccountId;
		/// <summary>
		/// AccountId
		/// </summary>
		[ModelAttribute(ColumnName = CNAccountId)]
		public System.String AccountId
		{
			get { return _AccountId; }
			set { _AccountId = value; }
		}

		private System.String _AccountName;
		/// <summary>
		/// AccountName
		/// </summary>
		[ModelAttribute(ColumnName = CNAccountName)]
		public System.String AccountName
		{
			get { return _AccountName; }
			set { _AccountName = value; }
		}

		private System.Decimal _Amount;
		/// <summary>
		/// Amount
		/// </summary>
		[ModelAttribute(ColumnName = CNAmount)]
		public System.Decimal Amount
		{
			get { return _Amount; }
			set { _Amount = value; }
		}

		private System.Decimal _Balance;
		/// <summary>
		/// Balance
		/// </summary>
		[ModelAttribute(ColumnName = CNBalance)]
		public System.Decimal Balance
		{
			get { return _Balance; }
			set { _Balance = value; }
		}

		private System.String _Remark;
		/// <summary>
		/// Remark
		/// </summary>
		[ModelAttribute(ColumnName = CNRemark)]
		public System.String Remark
		{
			get { return _Remark; }
			set { _Remark = value; }
		}

		private System.DateTime _ConsumeDate;
		/// <summary>
		/// ConsumeDate
		/// </summary>
		[ModelAttribute(ColumnName = CNConsumeDate)]
		public System.DateTime ConsumeDate
		{
			get { return _ConsumeDate; }
			set { _ConsumeDate = value; }
		}

		private System.Boolean _IsCancel;
		/// <summary>
		/// IsCancel
		/// </summary>
		[ModelAttribute(ColumnName = CNIsCancel)]
		public System.Boolean IsCancel
		{
			get { return _IsCancel; }
			set { _IsCancel = value; }
		}

		private System.DateTime _AddTime;
		/// <summary>
		/// AddTime
		/// </summary>
		[ModelAttribute(ColumnName = CNAddTime)]
		public System.DateTime AddTime
		{
			get { return _AddTime; }
			set { _AddTime = value; }
		}

		private System.DateTime _LastTime;
		/// <summary>
		/// LastTime
		/// </summary>
		[ModelAttribute(ColumnName = CNLastTime)]
		public System.DateTime LastTime
		{
			get { return _LastTime; }
			set { _LastTime = value; }
		}

		private System.String _OperatorId;
		/// <summary>
		/// OperatorId
		/// </summary>
		[ModelAttribute(ColumnName = CNOperatorId)]
		public System.String OperatorId
		{
			get { return _OperatorId; }
			set { _OperatorId = value; }
		}

		#region 静态的字段名的方法
		public const string CNId = "Id";
		public const string CNConsumeName = "ConsumeName";
		public const string CNConsumeTypeId = "ConsumeTypeId";
		public const string CNConsumeTypeName = "ConsumeTypeName";
		public const string CNBalanceTypeId = "BalanceTypeId";
		public const string CNBalanceTypeName = "BalanceTypeName";
		public const string CNAccountId = "AccountId";
		public const string CNAccountName = "AccountName";
		public const string CNAmount = "Amount";
		public const string CNBalance = "Balance";
		public const string CNRemark = "Remark";
		public const string CNConsumeDate = "ConsumeDate";
		public const string CNIsCancel = "IsCancel";
		public const string CNAddTime = "AddTime";
		public const string CNLastTime = "LastTime";
		public const string CNOperatorId = "OperatorId";
		#endregion

	}
}
