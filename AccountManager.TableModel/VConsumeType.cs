using System;
using System.Collections.Generic;
using System.Text;
using ZhCun.Framework.Common.Models.TableModel;

namespace AccountManager.TableModel.Model
{
	public class VConsumeType : ModelBase
	{
		private string _Id;
		/// <summary>
		/// Id
		/// </summary>
		[ModelAttribute(ColumnName = CNId, IsPrimaryKey = true, IsNotNull = true)]
		public string Id
		{
			get { return _Id; }
			set { _Id = value; }
		}

		private string _TypeName;
		/// <summary>
		/// TypeName
		/// </summary>
		[ModelAttribute(ColumnName = CNTypeName)]
		public string TypeName
		{
			get { return _TypeName; }
			set { _TypeName = value; }
		}

		private string _TypeNamePY;
		/// <summary>
		/// TypeNamePY
		/// </summary>
		[ModelAttribute(ColumnName = CNTypeNamePY)]
		public string TypeNamePY
		{
			get { return _TypeNamePY; }
			set { _TypeNamePY = value; }
		}

		private int _UseCount;
		/// <summary>
		/// UseCount
		/// </summary>
		[ModelAttribute(ColumnName = CNUseCount)]
		public int UseCount
		{
			get { return _UseCount; }
			set { _UseCount = value; }
		}

		private int _Category;
		/// <summary>
		/// Category
		/// </summary>
		[ModelAttribute(ColumnName = CNCategory)]
		public int Category
		{
			get { return _Category; }
			set { _Category = value; }
		}

		private bool _IsPublic;
		/// <summary>
		/// IsPublic
		/// </summary>
		[ModelAttribute(ColumnName = CNIsPublic)]
		public bool IsPublic
		{
			get { return _IsPublic; }
			set { _IsPublic = value; }
		}

		private string _CategoryName;
		/// <summary>
		/// CategoryName
		/// </summary>
		[ModelAttribute(ColumnName = CNCategoryName)]
		public string CategoryName
		{
			get { return _CategoryName; }
			set { _CategoryName = value; }
		}

		private string _Remark;
		/// <summary>
		/// Remark
		/// </summary>
		[ModelAttribute(ColumnName = CNRemark)]
		public string Remark
		{
			get { return _Remark; }
			set { _Remark = value; }
		}

		private string _OperatorId;
		/// <summary>
		/// OperatorId
		/// </summary>
		[ModelAttribute(ColumnName = CNOperatorId)]
		public string OperatorId
		{
			get { return _OperatorId; }
			set { _OperatorId = value; }
		}

		private string _OperatorName;
		/// <summary>
		/// OperatorName
		/// </summary>
		[ModelAttribute(ColumnName = CNOperatorName)]
		public string OperatorName
		{
			get { return _OperatorName; }
			set { _OperatorName = value; }
		}

		private DateTime _LastTime;
		/// <summary>
		/// LastTime
		/// </summary>
		[ModelAttribute(ColumnName = CNLastTime)]
		public DateTime LastTime
		{
			get { return _LastTime; }
			set { _LastTime = value; }
		}

		private DateTime _AddTime;
		/// <summary>
		/// AddTime
		/// </summary>
		[ModelAttribute(ColumnName = CNAddTime)]
		public DateTime AddTime
		{
			get { return _AddTime; }
			set { _AddTime = value; }
		}

		#region 静态的字段名的方法
		public const string CNId = "Id";
		public const string CNTypeName = "TypeName";
		public const string CNTypeNamePY = "TypeNamePY";
		public const string CNUseCount = "UseCount";
		public const string CNCategory = "Category";
		public const string CNIsPublic = "IsPublic";
		public const string CNCategoryName = "CategoryName";
		public const string CNRemark = "Remark";
		public const string CNOperatorId = "OperatorId";
		public const string CNOperatorName = "OperatorName";
		public const string CNLastTime = "LastTime";
		public const string CNAddTime = "AddTime";
		#endregion

	}
}
